output "anz_vpc_id" {
  value       = data.google_compute_network.default.id
  sensitive   = true
  description = "description"
}

output "anz_subnet_id" {
  value = google_compute_subnetwork.dev-subnet-2.id
}

output "anz_subnet_gateway" {
  value = google_compute_subnetwork.dev-subnet-2.gateway_address
}

output "subnet_1_name" {
  value       = google_compute_subnetwork.dev-subnet-1.name
  description = "description"
}
