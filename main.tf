

resource "google_compute_network" "development-vpc" {
  name                    = "development-vpc"
  auto_create_subnetworks = false
}

# VPC network information fetch
data "google_compute_network" "default" {
  name = "default"
}

# Subnet Network 1
resource "google_compute_subnetwork" "dev-subnet-1" {
  name          = var.subnet_ciders[0].name
  network       = google_compute_network.development-vpc.id
  ip_cidr_range = var.subnet_ciders[0].subnet_cider
  region        = "us-central1"
}

#Subnet Network 2
resource "google_compute_subnetwork" "dev-subnet-2" {
  name          = var.subnet_ciders[1].name
  network       = data.google_compute_network.default.id
  ip_cidr_range = var.subnet_ciders[1].subnet_cider
  region        = "us-central1"
}

# we can remove the part of resource using below command , but not a recommended way.
# $ terraform destroy -target google_compute_subnetwork.dev-subnet-2

# resource "google_sql_database_instance" "anz-prod" {
#   name             = "main-instance"
#   database_version = "POSTGRES_15"
#   region           = "us-central1"
#   settings {
#     # Second-generation instance tiers are based on the machine
#     # type. See argument reference below.
#     tier = "db-f1-micro"
#   }
# }